import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import PropTypes from "prop-types";

import "./Catalog.scss";

import Products from "../../Components/Products/Products";

const Catalog = ({ favorites, toggleFavorites, toggleCartProduct }) => {
    const [products, setProducts] = useState([]);
    const [isOpenModalCart, setIsOpenModalCart] = useState(false);
    const [currentCard, setcurrentCard] = useState({}); 
    const { category } = useParams(); 
    
    const toggleModalCart = () => {
        setIsOpenModalCart(!isOpenModalCart)
      };

    const handleCurrentPost = (card) => {
        setcurrentCard(card); 
    }

    useEffect(() => {
        fetch(`/data.json`)
            .then(response => {
                return response.json();    
            })
            .then(data => {
                setProducts(data[category]);
            })
    }, [category])

    return (
        <div className="catalog">
            <Products 
                products={products}
                favorites={favorites} 
                toggleFavorites={toggleFavorites} 
                toggleCartProduct={toggleCartProduct}
                isOpenModalCart={isOpenModalCart}
                toggleModalCart={toggleModalCart}
                currentCard={currentCard}
                handleCurrentPost={handleCurrentPost}
                category={category}/>
        </div>
    )
}

Products.propTypes = {
    favorites: PropTypes.array, 
    toggleFavorites: PropTypes.func,
    toggleCartProduct: PropTypes.func
  };

export default Catalog;
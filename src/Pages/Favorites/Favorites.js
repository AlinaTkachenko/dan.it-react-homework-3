import React, { useState } from 'react';
import "./Favorites.scss";

import Title from "../../Components/Title/Title";
import Products from "../../Components/Products/Products";

const Favorites = ({ favorites, toggleFavorites, toggleCartProduct }) => {

    const [isOpenModalCart, setIsOpenModalCart] = useState(false);
    const [currentCard, setcurrentCard] = useState({});

    const toggleModalCart = () => {
        setIsOpenModalCart(!isOpenModalCart)
      };

    const handleCurrentPost = (card) => {
        setcurrentCard(card); 
    }

    return (
        <div className="favorites">
            <div className="interior-content">
                <Title>Обране</Title>
                <Products 
                    products={favorites}
                    favorites={favorites}
                    toggleFavorites={toggleFavorites} 
                    toggleCartProduct={toggleCartProduct}
                    isOpenModalCart={isOpenModalCart}
                    currentCard={currentCard}
                    toggleModalCart={toggleModalCart}
                    handleCurrentPost={handleCurrentPost}/>
            </div>
        </div>
    )
}

export default Favorites; 
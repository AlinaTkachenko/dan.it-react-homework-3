import PropTypes from "prop-types";

import CartProductCard from "./CartProductCard";
import ModalText from "../../../Components/Modal/ModalText/ModalText";
import Empty from "../../../Components/Empty/Empty";

const CartProducts = ({cartProducts, toggleCartProduct, isOpenModalCart, toggleModalCart, currentCard, handleCurrentPost}) => {
    console.log(currentCard)
    return (
        <>
            <div className="cart__products">
                {cartProducts.length === 0 && <Empty />}
                {cartProducts.length > 0 && cartProducts.map((product)=> <CartProductCard 
                key={product.id}
                product={product}
                currentCard={currentCard} 
                toggleCartProduct={toggleCartProduct}
                toggleModalCart={toggleModalCart}
                handleCurrentPost={handleCurrentPost}></CartProductCard>)}
            </div>
            <ModalText 
                isOpenModalCart={isOpenModalCart} 
                toggleModalCart={toggleModalCart} 
                currentCard={currentCard} 
                toggleCartProduct={toggleCartProduct}
            />   
        </> 
    )  
}

CartProducts.propTypes = {
    cartProducts: PropTypes.object,
    toggleCartProduct: PropTypes.func,
    isOpenModalCart: PropTypes.bool,
    toggleModalCart: PropTypes.func,
    currentCard: PropTypes.object,
    handleCurrentPost: PropTypes.func
  };

export default CartProducts;
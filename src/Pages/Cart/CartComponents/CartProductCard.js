import PropTypes from "prop-types";

import ColorProductCard from "../../../Components/ProductCard/ComponentsProductCard/ColorProductCard";
import ImageProductCard from "../../../Components/ProductCard/ComponentsProductCard/ImageProductCard";
import PriceProductCard from "../../../Components/ProductCard/ComponentsProductCard/PriceProductCard";
import TitleProductCard from "../../../Components/ProductCard/ComponentsProductCard/TitleProductCard";
import Counter from "../../../Components/Counter/Counter";
import Button from "../../../Components/Button/Button";

const CartProductCard = ({product, toggleCartProduct, toggleModalCart, handleCurrentPost }) => { 
    const{src, title, color, price} = product;
    
    return (
        <div className="cart__product-card row">
            <ImageProductCard src={src} optionalClassNames="cart__image"/> 
            <div className="cart__wrapper">
                <div>
                    <TitleProductCard optionalClassNames="cart__title">{title}</TitleProductCard>
                    <ColorProductCard color={color}/>
                    <PriceProductCard price={price}/>
                </div>
                <Counter 
                    product={product} 
                    toggleCartProduct={toggleCartProduct} 
                    toggleModalCart={toggleModalCart}
                    handleCurrentPost={handleCurrentPost}/>
                <Button optionalClassNames="cart__button" 
                    onClick={()=>{
                        toggleModalCart();
                        handleCurrentPost(product);}
                    }>Видалити</Button>
            </div>       
        </div>
    )
}

CartProductCard.propTypes = {
    product: PropTypes.object,
    toggleCartProduct: PropTypes.func,
    isOpenModalCart: PropTypes.bool,
    toggleModalCart: PropTypes.func
  };

export default CartProductCard;
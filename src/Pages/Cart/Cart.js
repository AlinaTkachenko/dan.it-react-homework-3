import { useState } from "react"; 
import PropTypes from "prop-types";

import "./Cart.scss";

import Title from "../../Components/Title/Title";
import CartProducts from "./CartComponents/CartProducts";

const Cart = ({cartProducts, toggleCartProduct}) => {
    const [isOpenModalCart, setIsOpenModalCart] = useState(false); 
    const [currentCard, setcurrentCard] = useState({});

    const toggleModalCart = () => {
        setIsOpenModalCart(!isOpenModalCart)
    };

    const handleCurrentPost = (card) => {
        setcurrentCard(card); 
    }  

    return (
        <div className="cart">
            <div className="interior-content">
                <Title>Кошик</Title>
                <CartProducts 
                    cartProducts={cartProducts} 
                    toggleCartProduct={toggleCartProduct}
                    isOpenModalCart={isOpenModalCart}
                    toggleModalCart={toggleModalCart}
                    currentCard={currentCard}
                    handleCurrentPost={handleCurrentPost}/>
            </div>
        </div>
    )
}

CartProducts.propTypes = {
    cartProducts: PropTypes.array,
    toggleCartProduct: PropTypes.func
  };

export default Cart; 
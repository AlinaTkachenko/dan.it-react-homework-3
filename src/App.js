import React, { useState, useEffect } from 'react';
import { Routes, Route, Link } from 'react-router-dom';

import './App.scss';

import Layout from './Components/Layout/Layout';
import Main from './Pages/Main/Main';
import Catalog from './Pages/Catalog/Catalog';
import Product from './Pages/Product/Product';
import About from './Pages/About/About';
import Support from './Pages/Support/Support';
import Contacts from './Pages/Contacts/Contacts';
import Person from './Pages/Person/Person';
import Favorites from './Pages/Favorites/Favorites';
import Cart from './Pages/Cart/Cart';


function App() {
  const [favorites,setFavorites] = useState([]);
  const [cartProducts, setCartProducts] = useState([]);

  const toggleFavorites = (product) => {
    
    if(!favorites.find(favorite => favorite.id === product.id)) {
      const newFavorites = [...favorites, product];
      setFavorites(newFavorites);
      localStorage.favorites = JSON.stringify(newFavorites);
    }
    else {
      const newFavorites = favorites.filter(item => product.id != item.id);
      setFavorites(newFavorites);
      localStorage.favorites = JSON.stringify(newFavorites);
    }  
  }

  const toggleCartProduct = (product,action) => {

    let newCartProducts = [...cartProducts];

    if(!cartProducts.find(item => item.id === product.id)) {
      newCartProducts.push(product);
    }

    newCartProducts.map(item => { 
      if(item.id === product.id && action === 'plus') {
        item.count ? item.count++ : product.count = 1
      }
      if(item.id === product.id && action === 'minus') {
        item.count--
      }
    })

    if (action === 'remove') {
      newCartProducts = newCartProducts.filter(item => item.title != product.title)
    }

    setCartProducts(newCartProducts);
    localStorage.cardProducts = JSON.stringify(newCartProducts);
  }

  useEffect(() => {  
    if(localStorage.favorites) {
      const startFavorites = JSON.parse(localStorage.favorites);
      setFavorites(startFavorites);
    } 
    if(localStorage.cardProducts) {
      const startCardProducts = JSON.parse(localStorage.cardProducts);
      setCartProducts(startCardProducts);
    } 
  },[]);

  return (
    <div className="App">
          <Routes>
            <Route path="/" element={<Layout favorites={favorites} cartProducts={cartProducts}/>}>
              <Route index element={<Main />}/>
              <Route path="catalog/:category" element={<Catalog
                favorites={favorites} 
                toggleFavorites={toggleFavorites} 
                toggleCartProduct={toggleCartProduct}/>}/>
              <Route path="catalog/:category/:product" element={<Product />}/>
              <Route path="about" element={<About />}/>
              <Route path="support" element={<Support />}/>
              <Route path="contacts" element={<Contacts />}/>
              <Route path="person" element={<Person />}/>
              <Route path="cart" element={<Cart 
                cartProducts={cartProducts} 
                toggleCartProduct={toggleCartProduct}/>}/>
              <Route path="favorites" element={<Favorites 
                favorites={favorites}
                toggleFavorites={toggleFavorites}
                toggleCartProduct={toggleCartProduct} />}/>
            </Route>
          </Routes>
    </div>
  );
}

export default App;

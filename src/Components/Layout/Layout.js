import { Outlet} from 'react-router-dom';

import Header from '../Header/Header';
import Footer from '../Footer/Footer';

const Layout = ({favorites, cartProducts}) => {
    return (
        <div className='wrapper'>
            <Header favorites={favorites} cartProducts={cartProducts}/>
            <div className='main content'>
                <Outlet />
            </div>
            <Footer />
        </div>
    )
}

export default Layout;
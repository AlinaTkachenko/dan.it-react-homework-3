import "./Empty.scss";
import cn from "classnames";

const Empty = ({optionalClassNames}) => {
    return (
        <div className={cn("empty",optionalClassNames)}>
           Немає товару
        </div>
    )
}

export default Empty;
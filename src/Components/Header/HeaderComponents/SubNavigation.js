import NavigationItem from "./NavigationItem";

const SubNavigation = () => {
    const menuList = [{name: 'Велосипеди', path: '/catalog/bike'},
    {name: 'Контролери', path: '/catalog/controller'},
    {name: 'Акумулятори', path: '/catalog/accumulator'},
    {name: 'Двигуни', path: '/catalog/motor'},
    {name: 'Аксесуари', path: '/catalog/accessories'},
    {name: 'Електронабори', path: '/catalog/elektro-kits'}
]

    const menuItems = menuList.map((item, index) =>
    <NavigationItem key={index} to={item.path} name={item.name}/>
  );

    return (
        <nav className="sub-navigation">
            <ul className="sub-navigation__list">
                {menuItems}
            </ul>
        </nav>
    )
}
export default SubNavigation;
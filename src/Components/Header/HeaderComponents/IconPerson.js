import PropTypes from 'prop-types';

import Icon from "../../Icon/Icon";

const IconPerson = ({src,alt,width,height}) => {
    return (
        <Icon src={src} alt={alt} width={width} height={height} />
    )
}

IconPerson.propTypes = {
    src: PropTypes.string,
    alt: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string
  };

export default IconPerson;
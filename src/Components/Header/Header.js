import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

import "./Header.scss";

import Icon from "../Icon/Icon";
import Logo from "../Logo/Logo";
import Navigation from "./HeaderComponents/Navigation";
import Favorites from "./HeaderComponents/Favorites";
import IconCart from "./HeaderComponents/IconCart";
import IconPerson from './HeaderComponents/IconPerson';

const Header = ({favorites, cartProducts}) => {
    return(
        <div className="header">
            <div className="header__content content row">
                <Logo />
                <div className="header__wrapper row">
                    <Navigation />
                    <div className="header__icons row">
                        <Link to='/person'><IconPerson src="/images/icons/person.png" alt="person" width="24px" height="24px" /></Link>
                        <Link to='/cart'><IconCart cartProducts={cartProducts}/></Link>
                        <Link to='/favorites'><Favorites favorites={favorites}/></Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

Header.propTypes = {
    favorites: PropTypes.array,
    cartProducts: PropTypes.array
  };

export default Header;
import { useState } from 'react';
import PropTypes from "prop-types";

import "./Counter.scss";

const Counter = ({product, toggleCartProduct, toggleModalCart, handleCurrentPost}) => {
    const [counter, setCounter] = useState(product.count || 1);

    const increaseCount = () =>  {
        setCounter(+counter+1)
      }

    const decreaseCount = () => {
        if(counter > 1 ){
            setCounter(+counter-1)
        } 
      }  

    return (
        <div className="counter row">
            <span onClick={() => {
                decreaseCount();
                if(counter === 1) {
                    toggleModalCart();
                    handleCurrentPost(product);
                }else {
                    toggleCartProduct(product,'minus')
                }
            }
            }>
                <img src="./images/icons/minus.png"></img>
            </span>
            <input type="text" value={counter} readOnly/>
            <span onClick={() => {
                increaseCount();
                toggleCartProduct(product,'plus')
                }
                }>
                <img src="./images/icons/plus.png"></img>
            </span>
        </div>
    )
}

Counter.propTypes = {
    product: PropTypes.object,
    toggleCartProduct: PropTypes.func, 
    toggleModalCart: PropTypes.func, 
    handleCurrentPost: PropTypes.func
  };

export default Counter;
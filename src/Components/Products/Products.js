import PropTypes from "prop-types";

import "./Products.scss";

import ProductCard from "../ProductCard/ProductCard";
import ModalCart from '../Modal/ModalCart/ModalCart';
import Empty from "../Empty/Empty";

const Products = ({
    products, 
    favorites, 
    toggleFavorites, 
    toggleCartProduct, 
    isOpenModalCart, 
    currentCard, 
    toggleModalCart, 
    handleCurrentPost,
    category}) => {
    
    return (
        <>
            {products.length === 0 && <Empty />}
            <div className="products">
                {products.length > 0 && products.map((product)=><ProductCard 
                key={product.id} 
                product={product}
                favorites={favorites} 
                toggleFavorites={toggleFavorites}
                toggleModalCart={toggleModalCart}
                handleCurrentPost={handleCurrentPost}
                category={category}></ProductCard>)}
            </div>
            <ModalCart 
                isOpenModalCart={isOpenModalCart} 
                toggleModalCart={toggleModalCart} 
                currentCard={currentCard} 
                toggleCartProduct={toggleCartProduct}
            />
        </>
    )
}

Products.propTypes = {
    toggleCartProduct: PropTypes.func,
    toggleFavorites: PropTypes.func,
    favorites: PropTypes.array, 
    products: PropTypes.array,
    isOpenModalCart: PropTypes.bool,
    currentCard: PropTypes.object,
    toggleModalCart: PropTypes.func,
    handleCurrentPost: PropTypes.func
  };

export default Products;
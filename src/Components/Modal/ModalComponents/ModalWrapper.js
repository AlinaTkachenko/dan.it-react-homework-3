import './ModalWrapper.scss';
import PropTypes from 'prop-types';

const ModalWrapper = ({isOpen, closeModal, children}) => {

    return (
        <>
        {isOpen && (
            <div className='modal-wrapper' onClick={closeModal}>{children}</div>
        )}
        </>
        
    )
}

ModalWrapper.propTypes = {
    isOpen: PropTypes.bool,
    closeModal: PropTypes.func
  };

export default ModalWrapper;
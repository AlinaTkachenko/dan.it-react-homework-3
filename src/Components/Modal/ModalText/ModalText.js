import PropTypes from "prop-types";

import ModalWrapper from "./../ModalComponents/ModalWrapper"
import Modal from "./../ModalComponents/Modal";
import ModalHeader from "./../ModalComponents/ModalHeader";
import ModalBody from "./../ModalComponents/ModalBody";
import ModalImg from "./../ModalComponents/ModalImg";
import ModalFooter from "./../ModalComponents/ModalFooter";

const ModalText = ({isOpenModalCart, toggleModalCart, currentCard, toggleCartProduct}) => {
    const { src, title } = currentCard;
    return (
        <ModalWrapper isOpen={isOpenModalCart}>
            <Modal closeModal={toggleModalCart}>
                <ModalImg src={src} alt={title}></ModalImg>
                <ModalHeader>{title}</ModalHeader>
                <ModalBody optionalClassName="body-price">Бажаєте видалити цей товар?</ModalBody>
                <ModalFooter 
                    firstText="Так"
                    secondaryText="Ні" 
                    firstClick = {() => {
                        toggleCartProduct(currentCard, "remove")
                        toggleModalCart();
                    }}
                    secondaryClick = {toggleModalCart}/>
            </Modal>
        </ModalWrapper>
    )
}

ModalText.propTypes = {
    isOpenModalCart: PropTypes.bool,
    toggleModalCart: PropTypes.func,
    toggleCartProduct: PropTypes.func,
    currentCard: PropTypes.object
  };

export default ModalText;
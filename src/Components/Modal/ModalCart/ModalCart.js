import PropTypes from "prop-types";

import ModalWrapper from "./../ModalComponents/ModalWrapper"
import Modal from "./../ModalComponents/Modal";
import ModalHeader from "./../ModalComponents/ModalHeader";
import ModalBody from "./../ModalComponents/ModalBody";
import ModalImg from "./../ModalComponents/ModalImg";
import ModalFooter from "./../ModalComponents/ModalFooter";

const ModalCart = ({isOpenModalCart, toggleModalCart, currentCard, toggleCartProduct}) => {
    const { src, title, price } = currentCard;
    return (
        <ModalWrapper isOpen={isOpenModalCart}>
            <Modal closeModal={toggleModalCart}>
                <ModalImg src={src} alt={title}></ModalImg>
                <ModalHeader>{title}</ModalHeader>
                <ModalBody optionalClassName="body-price">{price} грн.</ModalBody>
                <ModalFooter 
                    firstText="Додати до кошика" 
                    firstClick={() => {
                        toggleCartProduct(currentCard, "plus")
                        toggleModalCart();
                    }}/>
            </Modal>
        </ModalWrapper>
    )
}

ModalCart.propTypes = {
    isOpenModalCart: PropTypes.bool,
    toggleModalCart: PropTypes.func,
    toggleCartProduct: PropTypes.func,
    currentCard: PropTypes.object
  };

export default ModalCart;
import "../Contacts.scss";

import Icon from "../../Icon/Icon";

const Address = () => {
    return(
        <div className="address row">
           <Icon src="/images/icons/address.png" alt="location" height="18px"/> 
           <span>Київ, проспект Степана Бандери 15Г </span>
        </div>
    )
}

export default Address;
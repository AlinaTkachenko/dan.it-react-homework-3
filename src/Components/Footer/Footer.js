import "./Footer.scss"

import Logo from "../Logo/Logo";
import Address from "../Contacts/ContactsComponents/Address";
import Phones from "../Contacts/ContactsComponents/Phones";
import Social from "../Contacts/ContactsComponents/Social";

const Footer = () => {
    return(
        <div className="footer">
            <div className="footer__content content row">
                <Logo />
                <div>
                    <Address />
                    <div className="row">
                        <Phones />
                        <Social />
                    </div>
                </div> 
            </div>
        </div>
    )
}

export default Footer;
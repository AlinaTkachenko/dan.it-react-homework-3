const TitleProductCard = ({children, optionalClassNames}) => {
    return (
        <div className={optionalClassNames}>
            {children}
        </div>
    )
}

export default TitleProductCard;
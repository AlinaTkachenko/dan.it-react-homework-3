import {Link} from "react-router-dom";
import PropTypes from "prop-types";
import cn from "classnames";

import "./ProductCard.scss";

import Button from "../Button/Button";
import ColorProductCard from "./ComponentsProductCard/ColorProductCard";
import ImageProductCard from "./ComponentsProductCard/ImageProductCard";
import PriceProductCard from "./ComponentsProductCard/PriceProductCard";
import TitleProductCard from "./ComponentsProductCard/TitleProductCard";
import {ReactComponent as Heart} from "../Svg/heart.svg";

const ProductCard = ({product, toggleFavorites, favorites, toggleModalCart, handleCurrentPost, category}) => {
    const{src, title, color, price} = product;

    const toggleActive = (event) => {
        event.target.closest("div").classList.toggle("active"); 
    }

    const addActiveClass = () => {
        const arrId = favorites.map(item=>item.id);
        if(arrId.includes(product.id)){
            return "active";
        }
    }

    return (
        <div className="product-card">
            <Link className="product-card__link"to={`/catalog/${category}/${product.id}`}>
                <ImageProductCard src={src} optionalClassNames="product-card__image"/>
                <TitleProductCard optionalClassNames="product-card__title">{title}</TitleProductCard>
                <ColorProductCard color={color}/>
            </Link>  
            <div className="product-card__wrapper row">
                <PriceProductCard price={price}/>
                <div className="product-card__cart-favorites">
                    <div className={cn("product-card__heart",addActiveClass())} id={product.id} onClick={(event)=> {
                        toggleFavorites(product)
                        toggleActive(event)
                    }}>
                        <Heart />
                    </div>
                    <Button src="/images/icons/cart-grey.png" 
                        optionalClassNames="product-card__button" 
                        onClick={()=>{
                            toggleModalCart()
                            handleCurrentPost(product)}}>
                    </Button>
                </div>
            </div>          
        </div>
    )
}

ProductCard.propTypes = {
    product: PropTypes.object,
    toggleFavorites: PropTypes.func,
    favorites: PropTypes.array, 
    toggleModalCart: PropTypes.func, 
    handleCurrentPost: PropTypes.func
  };

export default ProductCard;